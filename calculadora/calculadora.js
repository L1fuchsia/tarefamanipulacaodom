var btn= document.querySelector(".btn")
var NumOfGrades= 0
var SumOfGrades= 0
var calcbtn= document.querySelector(".calcbtn")
let input = document.querySelector(".grade");

function GetGrade(){
    let grade=document.querySelector(".grade").value
    grade.innerText = input.value;
    grade=parseFloat(grade)
    var Isvalid = !((!(grade>=0)&&!(grade<=0))||(grade>10)||grade<0)
    if(!Isvalid){
        alert('Insira um número de 1 a 10!!')
    } 
    if(Isvalid){
        return grade
    }
}
function AddGrade(){
    let linha = ''
    var grades=document.querySelector(".grades")
    let grade=GetGrade()
    if(grade!=undefined){
        NumOfGrades+=1
        SumOfGrades+=grade
        console.log('media atual: '+SumOfGrades/NumOfGrades)
        linha+='A nota '
        linha+=NumOfGrades
        linha+=' foi '
        linha+=grade
        linha+='\n'
        grades.innerHTML+=linha    
    }
    clear_input();

}

function printMedia(){

    let mediaTexto = document.querySelector('h2')
    mediaTexto.innerHTML='A média é: ' + SumOfGrades / NumOfGrades

}

function clear_input() {
    input.value = "";
}

function clear_message() {
    location. reload()
}

calcbtn.addEventListener("click",printMedia)
btn.addEventListener("click",AddGrade)

let btn_clear = document.querySelector("#btn_clear");
btn_clear.addEventListener("click", clear_message);


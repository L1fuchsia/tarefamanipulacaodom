let section = document.querySelector(".section_message");
let input = document.querySelector("#input_message");

function send_message() {
    let escopo = document.createElement("div");
    escopo.classList.add("msg");

    
    let texto = document.createElement("p");
    texto.rows = 5;
    texto.cols = 50;
    texto.innerText = input.value;
    texto.disabled = false;
    escopo.appendChild(texto);
    
    let buttonEdit = document.createElement("button");
    buttonEdit.classList.add("edit-button")
    buttonEdit.type = "button";
    buttonEdit.textContent = "Editar";
    buttonEdit.addEventListener("click", ()=>(texto.innerHTML = prompt("Qual é a nova mensagem? " )));

    escopo.appendChild(buttonEdit);

    let buttonDelete = document.createElement("button");
    buttonDelete.classList.add("delete-button")
    buttonDelete.type = "button";
    buttonDelete.textContent = "Deletar";
    buttonDelete.addEventListener("click", ()=>(localmsg.remove()));
    escopo.appendChild(buttonDelete);
    let localmsg = texto.parentNode 
    section.append(escopo);
    clear_input();

}

function clear_message() {
    section.innerHTML = "";
}

function clear_input() {
    input.value = "";
}


let btn_send = document.querySelector("#btn_send");
btn_send.addEventListener("click", send_message);

let btn_clear = document.querySelector("#btn_clear");
btn_clear.addEventListener("click", clear_message);